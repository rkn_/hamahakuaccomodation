--
-- PostgreSQL database dump
--

-- Dumped from database version 11.5
-- Dumped by pg_dump version 11.5

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: itemspoint; Type: TABLE; Schema: public; Owner: lkeix
--

CREATE TABLE public.itemspoint (
    onehouse integer,
    guesthouse integer,
    jproom integer,
    jproom6 integer,
    jproom1 integer,
    jproom3woman integer,
    bedtype integer,
    singlewoman integer,
    shower integer,
    bath integer,
    wifi integer,
    kitchen integer,
    aircon integer,
    tv integer,
    weaklegs integer,
    closetoilet integer,
    reservedroom integer,
    traincamp integer,
    weakheart integer,
    kayabuki integer,
    kwarabuki integer
);


ALTER TABLE public.itemspoint OWNER TO lkeix;

--
-- Name: open; Type: TABLE; Schema: public; Owner: lkeix
--

CREATE TABLE public.open (
    rentid character varying(64),
    date date,
    rooms integer
);


ALTER TABLE public.open OWNER TO lkeix;

--
-- Name: rent; Type: TABLE; Schema: public; Owner: lkeix
--

CREATE TABLE public.rent (
    rentid character varying(64) NOT NULL,
    onehouse boolean,
    guesthouse boolean,
    jproom boolean,
    jproom6 boolean,
    jproom1 boolean,
    jproom3woman boolean,
    bedtype boolean,
    singlewoman boolean,
    shower boolean,
    bath boolean,
    wifi boolean,
    kitchen boolean,
    aircon boolean,
    tv boolean,
    weaklegs boolean,
    closetoilet boolean,
    reservedroom boolean,
    traincamp boolean,
    weakheart boolean,
    explain character varying(512),
    kayabuki boolean,
    kawarabuki boolean,
    name character varying(64)
);


ALTER TABLE public.rent OWNER TO lkeix;

--
-- Name: rent_plan; Type: TABLE; Schema: public; Owner: lkeix
--

CREATE TABLE public.rent_plan (
    rentid character varying(64),
    explain character varying(512),
    plan character varying(256)
);


ALTER TABLE public.rent_plan OWNER TO lkeix;

--
-- Name: rentinfo; Type: TABLE; Schema: public; Owner: lkeix
--

CREATE TABLE public.rentinfo (
    rentid character varying(64),
    photolink character varying(516)
);


ALTER TABLE public.rentinfo OWNER TO lkeix;

--
-- Name: rents; Type: TABLE; Schema: public; Owner: lkeix
--

CREATE TABLE public.rents (
    rentid character varying(64) NOT NULL,
    email character varying(32),
    tel character varying(16),
    name character varying(64),
    passwd character varying(256)
);


ALTER TABLE public.rents OWNER TO lkeix;

--
-- Name: reservation; Type: TABLE; Schema: public; Owner: lkeix
--

CREATE TABLE public.reservation (
    userid character varying(64),
    rentid character varying(64),
    checkin date,
    checkout date,
    rooms integer,
    man integer,
    woman integer
);


ALTER TABLE public.reservation OWNER TO lkeix;

--
-- Name: resister; Type: TABLE; Schema: public; Owner: lkeix
--

CREATE TABLE public.resister (
    userid character varying(64),
    rentid character varying(64),
    checkin date,
    checkout date,
    rooms integer,
    woman integer,
    man integer
);


ALTER TABLE public.resister OWNER TO lkeix;

--
-- Name: resister_info; Type: TABLE; Schema: public; Owner: lkeix
--

CREATE TABLE public.resister_info (
    userid character varying(64),
    rentid character varying(64),
    other character varying(128),
    alchohole boolean,
    english boolean,
    haral boolean,
    vegan boolean,
    vegetarian boolean,
    allergies character varying(512),
    plan character varying(256),
    point integer,
    weakheart boolean,
    weaklegs boolean,
    closetoilet boolean,
    reserved boolean,
    traincamp boolean
);


ALTER TABLE public.resister_info OWNER TO lkeix;

--
-- Name: user_auth_log; Type: TABLE; Schema: public; Owner: lkeix
--

CREATE TABLE public.user_auth_log (
    userid character varying(64),
    passwd character varying(255),
    sessid character varying(255),
    log timestamp without time zone
);


ALTER TABLE public.user_auth_log OWNER TO lkeix;

--
-- Name: users; Type: TABLE; Schema: public; Owner: lkeix
--

CREATE TABLE public.users (
    userid character varying(64) NOT NULL,
    email character varying(64),
    tel character varying(16),
    birth date,
    gender integer,
    firstname character varying(32),
    lastname character varying(32)
);


ALTER TABLE public.users OWNER TO lkeix;

--
-- Data for Name: itemspoint; Type: TABLE DATA; Schema: public; Owner: lkeix
--

COPY public.itemspoint (onehouse, guesthouse, jproom, jproom6, jproom1, jproom3woman, bedtype, singlewoman, shower, bath, wifi, kitchen, aircon, tv, weaklegs, closetoilet, reservedroom, traincamp, weakheart, kayabuki, kwarabuki) FROM stdin;
\.


--
-- Data for Name: open; Type: TABLE DATA; Schema: public; Owner: lkeix
--

COPY public.open (rentid, date, rooms) FROM stdin;
2NvO2dDL8uxfjlv	2019-10-10	2
2NvO2dDL8uxfjlv	2019-10-12	3
2NvO2dDL8uxfjlv	2019-10-17	1
2NvO2dDL8uxfjlv	2019-10-24	1
2NvO2dDL8uxfjlv	2019-11-03	2
2NvO2dDL8uxfjlv	2019-11-13	1
2NvO2dDL8uxfjlv	2019-11-14	1
2NvO2dDL8uxfjlv	2019-11-21	2
2NvO2dDL8uxfjlv	2019-11-29	3
2NvO2dDL8uxfjlv	2019-11-30	3
2NvO2dDL8uxfjlv	2019-11-04	1
\.


--
-- Data for Name: rent; Type: TABLE DATA; Schema: public; Owner: lkeix
--

COPY public.rent (rentid, onehouse, guesthouse, jproom, jproom6, jproom1, jproom3woman, bedtype, singlewoman, shower, bath, wifi, kitchen, aircon, tv, weaklegs, closetoilet, reservedroom, traincamp, weakheart, explain, kayabuki, kawarabuki, name) FROM stdin;
2NvO2dDL8uxfjlv	t	t	t	f	t	t	t	t	t	t	t	t	t	t	t	f	t	f	f	aaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaaa	t	t	
\.


--
-- Data for Name: rent_plan; Type: TABLE DATA; Schema: public; Owner: lkeix
--

COPY public.rent_plan (rentid, explain, plan) FROM stdin;
2NvO2dDL8uxfjlv		3
\.


--
-- Data for Name: rentinfo; Type: TABLE DATA; Schema: public; Owner: lkeix
--

COPY public.rentinfo (rentid, photolink) FROM stdin;
\.


--
-- Data for Name: rents; Type: TABLE DATA; Schema: public; Owner: lkeix
--

COPY public.rents (rentid, email, tel, name, passwd) FROM stdin;
2NvO2dDL8uxfjlv	test@test.test	090-0293-0495	test test	$2a$10$mNTXUewkdViGzBc2Zie.LO4rsMs2RdHE2zyJ9Jggb6jnYIv2.Qlgi
\.


--
-- Data for Name: reservation; Type: TABLE DATA; Schema: public; Owner: lkeix
--

COPY public.reservation (userid, rentid, checkin, checkout, rooms, man, woman) FROM stdin;
kDSQkxVwVwST5Ko	2NvO2dDL8uxfjlv	2019-11-10	2019-11-11	1	1	1
\.


--
-- Data for Name: resister; Type: TABLE DATA; Schema: public; Owner: lkeix
--

COPY public.resister (userid, rentid, checkin, checkout, rooms, woman, man) FROM stdin;
\.


--
-- Data for Name: resister_info; Type: TABLE DATA; Schema: public; Owner: lkeix
--

COPY public.resister_info (userid, rentid, other, alchohole, english, haral, vegan, vegetarian, allergies, plan, point, weakheart, weaklegs, closetoilet, reserved, traincamp) FROM stdin;
kDSQkxVwVwST5Ko	2NvO2dDL8uxfjlv		t	t	t	t	t	true		50	t	t	t	t	t
\.


--
-- Data for Name: user_auth_log; Type: TABLE DATA; Schema: public; Owner: lkeix
--

COPY public.user_auth_log (userid, passwd, sessid, log) FROM stdin;
kDSQkxVwVwST5Ko	$2a$10$ot4S3J9WLnMvSeZg8YVNxe/SwFQY6yfLNbMVqdsgRmZ3BXSzOw0O6	p8ztcktfzrreHq8	2019-11-02 00:00:00
\.


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: lkeix
--

COPY public.users (userid, email, tel, birth, gender, firstname, lastname) FROM stdin;
kDSQkxVwVwST5Ko	test@test.test	093-0394-9458	1933-08-17	1	test	test
\.


--
-- Name: rent rent_pkey; Type: CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.rent
    ADD CONSTRAINT rent_pkey PRIMARY KEY (rentid);


--
-- Name: rents rents_email_unique; Type: CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.rents
    ADD CONSTRAINT rents_email_unique UNIQUE (email);


--
-- Name: rents rents_pkey; Type: CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.rents
    ADD CONSTRAINT rents_pkey PRIMARY KEY (rentid);


--
-- Name: users users_pkey; Type: CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.users
    ADD CONSTRAINT users_pkey PRIMARY KEY (userid);


--
-- Name: open open_rentid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.open
    ADD CONSTRAINT open_rentid_fkey1 FOREIGN KEY (rentid) REFERENCES public.rents(rentid);


--
-- Name: rent_plan rent_plan_rentid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.rent_plan
    ADD CONSTRAINT rent_plan_rentid_fkey1 FOREIGN KEY (rentid) REFERENCES public.rents(rentid);


--
-- Name: rent rent_rentid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.rent
    ADD CONSTRAINT rent_rentid_fkey FOREIGN KEY (rentid) REFERENCES public.rents(rentid);


--
-- Name: rentinfo rentinfo_rentid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.rentinfo
    ADD CONSTRAINT rentinfo_rentid_fkey1 FOREIGN KEY (rentid) REFERENCES public.rents(rentid);


--
-- Name: reservation reservation_rentid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_rentid_fkey1 FOREIGN KEY (rentid) REFERENCES public.rents(rentid);


--
-- Name: reservation reservation_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.reservation
    ADD CONSTRAINT reservation_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(userid);


--
-- Name: resister_info resister_info_rentid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.resister_info
    ADD CONSTRAINT resister_info_rentid_fkey1 FOREIGN KEY (rentid) REFERENCES public.rents(rentid);


--
-- Name: resister_info resister_info_rentid_fkey2; Type: FK CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.resister_info
    ADD CONSTRAINT resister_info_rentid_fkey2 FOREIGN KEY (rentid) REFERENCES public.rents(rentid);


--
-- Name: resister_info resister_info_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.resister_info
    ADD CONSTRAINT resister_info_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(userid);


--
-- Name: resister resister_rentid_fkey1; Type: FK CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.resister
    ADD CONSTRAINT resister_rentid_fkey1 FOREIGN KEY (rentid) REFERENCES public.rents(rentid);


--
-- Name: resister resister_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.resister
    ADD CONSTRAINT resister_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(userid);


--
-- Name: user_auth_log user_auth_log_userid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: lkeix
--

ALTER TABLE ONLY public.user_auth_log
    ADD CONSTRAINT user_auth_log_userid_fkey FOREIGN KEY (userid) REFERENCES public.users(userid);


--
-- PostgreSQL database dump complete
--

