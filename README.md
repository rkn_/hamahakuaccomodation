# hamahaku

## Project setup
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Compiles and minifies for production
```
npm run build
```

### Run your tests
```
npm run test
```

### Lints and fixes files
```
npm run lint
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).

## launch at go

```
npm run build
go run src/api/serve.go
```

## postgresql

### mac
```
# 導入
brew install postgresql
initdb /usr/local/var/postgres -E utf8

# サーバの起動
postgres -D /usr/local/var/postgres

```

Setting
```
createuser -P <username>

createdb hamahaku -O <username>
psql -f ./db.sql -U <username> -d hamahaku
```

### windows
Postgres の bin のパスを通す
PG_DATAのパスを作る
初期 pw
postgres
```
# create user
createuser -U postgres -P lkeix

# create db
createdb -E UTF8 -O lkeix -U postgres hamahaku

# create tables
psql -f ./db.sql -U <username> -d hamahaku

# dump sql
pg_dump -h localhost -U lkeix -b -Fp hamahaku > db.sql

```

## Go
```
# postgres package
go get github.com/lib/pq
```