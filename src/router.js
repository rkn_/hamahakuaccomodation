import Vue from 'vue'
import VueRouter from 'vue-router'
import login from '@/views/login'
import control from '@/views/control'
import accomodationInfo from '@/views/accomodationinfo'
import signup from '@/views/signup'
import edit from '@/views/edit'

Vue.use(VueRouter)

const routes = [
  {
    path: '/login',
    component: login,
    name: 'login',
    meta: {
      isPublic: true
    }
  },
  {
    path: '/signup',
    component: signup,
    name: 'signup',
    meta: {
      isPublic: true
    }
  },
  {
    path: '/logged/control',
    component: control,
    name: 'control'
  },
  {
    path: '/logged/accomodationinfo/',
    component: accomodationInfo,
    name: 'accomodationinfo'
  },
  {
    path: '/logged/accomodationedit/',
    component: edit,
    name: 'accomodationedit'
  }
]

export default new VueRouter({
  routes: routes
})