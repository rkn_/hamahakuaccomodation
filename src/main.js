import Vue from 'vue'
import App from './App.vue'
import router from './router'
import vuetify from './plugins/vuetify'
import Vuex from 'vuex'
import axios from 'axios'
import store from './store'

Vue.use(vuetify)
Vue.use(Vuex)

Vue.config.productionTip = false

router.beforeEach((to, from, next) => {
  let params = new URLSearchParams()
  params.append('sessid', store.state.auth.sessid)
  axios.post('/hamahakuaccomodation/service/sessauth', params).then(res => {
    let sess = res.data
    if (to.matched.some(page => page.meta.isPublic) || sess || store.state.auth.login) {
      next()
    } else {
      next('/login')
    }
  })
})


new Vue({
  store,
  router,
  vuetify,
  render: h => h(App)
}).$mount('#app')
