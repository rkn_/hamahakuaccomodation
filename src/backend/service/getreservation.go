package service

import (
	"encoding/json"
	"hamahakuaccomodation/src/backend/libs"
	"time"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// ReservationInfo is a data struct reservation
type ReservationInfo struct {
	Name        string
	Checkin     string
	Checkout    string
	Rooms       int64
	Man         int64
	Woman       int64
	Alcohole    bool
	English     bool
	Haral       bool
	Vegan       bool
	Vegetarian  bool
	Allergies   string
	Plan        string
	Mail        string
	Subject     string
	Weakheart   bool
	Weaklegs    bool
	Closetoilet bool
	Reserved    bool
	Traincamp   bool
}

// GetReservation get decided reservation
func GetReservation(g *gin.Context) {
	session := sessions.Default(g)
	rentid := session.Get("rentid").(string)
	reservations := libs.GetReservation(rentid)
	var res []ReservationInfo
	layout := "2006/01/02"
	for _, ary := range reservations {
		var reservation ReservationInfo
		userid := ary[0].(string)
		userinfo := libs.GetReservationinfo(userid, rentid)[0]
		reservation.Name = libs.GetUserName(userid)
		reservation.Mail = libs.GetUserMail(userid)
		reservation.Rooms = ary[1].(int64)
		reservation.Checkin = ary[2].(time.Time).Format(layout)
		reservation.Checkout = ary[3].(time.Time).Format(layout)
		reservation.Man = ary[4].(int64)
		reservation.Woman = ary[5].(int64)
		reservation.Alcohole = userinfo[0].(bool)
		reservation.English = userinfo[1].(bool)
		reservation.Haral = userinfo[2].(bool)
		reservation.Vegan = userinfo[3].(bool)
		reservation.Vegetarian = userinfo[4].(bool)
		reservation.Allergies = userinfo[5].(string)
		reservation.Plan = userinfo[6].(string)
		reservation.Weakheart = userinfo[7].(bool)
		reservation.Weaklegs = userinfo[8].(bool)
		reservation.Closetoilet = userinfo[9].(bool)
		reservation.Reserved = userinfo[10].(bool)
		reservation.Traincamp = userinfo[11].(bool)
		res = append(res, reservation)
	}
	js, _ := json.Marshal(res)
	g.Writer.Write(js)
}
