package service

import (
	"encoding/json"
	"fmt"
	"hamahakuaccomodation/src/backend/libs"
	"strconv"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// Open is accmodation open struct
type Open struct {
	Dates []string
	Rooms []string
}

// UpdateOpen update open
func UpdateOpen(g *gin.Context) {
	session := sessions.Default(g)
	var open Open
	req := g.Request
	req.ParseForm()
	jsonstr := req.PostFormValue("data")
	fmt.Println(jsonstr)
	byt := []byte(jsonstr)
	json.Unmarshal(byt, &open)
	rentid := session.Get("rentid").(string)
	libs.InitOpen(rentid)
	for idx := range open.Dates {
		room, _ := strconv.Atoi(open.Rooms[idx])
		libs.InsertOpen(rentid, open.Dates[idx], room)
	}
}
