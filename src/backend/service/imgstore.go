package service

import (
	"encoding/json"
	"os"
	"path/filepath"
	"strconv"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
	"github.com/olahol/go-imageupload"
)

// Res is a struct which is written at img stored
type Res struct {
	Rentid string
	Num    int
}

// ImageStore store image
func ImageStore(g *gin.Context) {
	session := sessions.Default(g)
	rentid := session.Get("rentid").(string)
	initialize(rentid)
	var res Res
	req := g.Request
	img, err := imageupload.Process(req, "img")
	if err != nil {
		panic(err)
	}
	thumb, err := imageupload.ThumbnailPNG(img, 300, 300)

	if err != nil {
		panic(err)
	}
	gopath := os.Getenv("GOPATH")

	path := gopath + "/src/hamahakuaccomodation/data/" + rentid
	files, _ := filepath.Glob(path + "/*.jpg")
	cnt := len(files) + 1
	err = thumb.Save(path + "/" + strconv.Itoa(cnt) + ".jpg")
	if err != nil {
		panic(err)
	}
	res.Num = len(files)
	res.Rentid = rentid
	resbyte, err := json.Marshal(res)
	g.Writer.Write(resbyte)
}

// Getimg get image
func Getimg(g *gin.Context) {
	session := sessions.Default(g)
	rentid := session.Get("rentid").(string)
	initialize(rentid)
	var res Res
	gopath := os.Getenv("GOPATH")

	path := gopath + "/src/hamahakuaccomodation/data/" + rentid
	files, _ := filepath.Glob(path + "/*.jpg")
	res.Num = len(files)
	res.Rentid = rentid
	resbyte, _ := json.Marshal(res)
	g.Writer.Write(resbyte)
}

func initialize(rentid string) {
	gopath := os.Getenv("GOPATH")

	path := gopath + "/src/hamahakuaccomodation/data/" + rentid
	f, err := os.Stat(path)
	if os.IsNotExist(err) || !f.IsDir() {
		os.MkdirAll(path, 0755)
	}
}
