package service

import (
	"encoding/json"
	"hamahakuaccomodation/src/backend/libs"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// AuthRes response struct
type AuthRes struct {
	Auth   bool
	Sessid string
}

// Auth process login
func Auth(g *gin.Context) {
	session := sessions.Default(g)
	req := g.Request
	req.ParseForm()

	mail := req.PostFormValue("mail")
	password := req.PostFormValue("password")
	hashed, rentid := libs.GetAuthElm(mail)
	var res AuthRes
	sessid := libs.GenerateID()
	if libs.ComparePasswd(password, hashed) {
		res.Auth = true
		res.Sessid = sessid
		session.Set("rentid", rentid)
		session.Set("sessid", sessid)
		session.Save()
	}
	js, _ := json.Marshal(res)
	g.Writer.Write(js)
}
