package service

import (
	"encoding/json"

	// libs need access PostgreSQL
	"hamahakuaccomodation/src/backend/libs"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

type signupreq struct {
	Accomodationname string
	Mail             string
	Tel              string
	Passwd           string
	Passwordconfirm  string
}

type response struct {
	Ok     bool
	Sessid string
}

// Signup process sign up request
func Signup(g *gin.Context) {
	req := g.Request
	session := sessions.Default(g)
	req.ParseForm()
	data := req.PostFormValue("data")
	var reqdata signupreq
	json.Unmarshal(([]byte)(data), &reqdata)
	passwd := libs.CryptPasswd(reqdata.Passwd)
	result, rentid := libs.ResisterUser(reqdata.Accomodationname, reqdata.Mail, reqdata.Tel, passwd)
	var serverres response
	serverres.Ok = result
	if result {
		serverres.Sessid = libs.GenerateID()
		res, _ := json.Marshal(serverres)
		session.Set("sessid", serverres.Sessid)
		session.Set("rentid", rentid)
		session.Save()
		g.Writer.Write(res)
	} else {
		res, _ := json.Marshal(serverres)
		g.Writer.Write(res)
	}
}
