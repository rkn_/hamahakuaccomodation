package service

import (
	"encoding/json"
	"fmt"
	"hamahakuaccomodation/src/backend/libs"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// Openres response data struct
type Openres struct {
	Dates []string
	Rooms []int64
}

// GetExert get exert date
func GetExert(g *gin.Context) {
	req := g.Request
	session := sessions.Default(g)
	req.ParseForm()
	rentid := session.Get("rentid").(string)
	dates := libs.GetOpen(rentid)
	var open Openres
	for _, ary := range dates {
		date := ary[0].(string)
		room := ary[1].(int64)
		open.Dates = append(open.Dates, date)
		open.Rooms = append(open.Rooms, room)
	}
	js, _ := json.Marshal(open)
	fmt.Println(string(js))
	g.Writer.Write(js)
}
