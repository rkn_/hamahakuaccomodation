package service

import (
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// SessAuth authorize session
func SessAuth(g *gin.Context) {
	req := g.Request
	req.ParseForm()
	sessid := req.PostFormValue("sessid")
	session := sessions.Default(g)
	if session.Get("sessid") == nil {
		g.Writer.WriteString("false")
		return
	}
	if sessid == session.Get("sessid").(string) {
		g.Writer.WriteString("true")
		return
	}
	g.Writer.WriteString("false")
}
