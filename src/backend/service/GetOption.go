package service

import (
	"encoding/json"
	"fmt"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"

	// libs is Library
	"hamahakuaccomodation/src/backend/libs"
	"hamahakuaccomodation/src/backend/model"
)

type resdata struct {
	Onehouse      bool
	Japaneseroom  bool
	Bedtype       bool
	Wifi          bool
	Guesthouse    bool
	Japaneseroom6 bool
	Singleroom    bool
	Kitchen       bool
	Kayabuki      bool
	Japaneseroom1 bool
	Shower        bool
	Aircon        bool
	Kawarabuki    bool
	Japaneseroom3 bool
	Bath          bool
	Tv            bool
	Floor         bool
	Reserved      bool
	Near          bool
	Traincamp     bool
	Domitory      bool
	Explain       string
	Plans         []string
}

// GetOption get accomodation option
func GetOption(g *gin.Context) {
	// rentid などはセッション辺りから取得する予定
	session := sessions.Default(g)
	rentid := session.Get("rentid").(string)
	var exed model.ExecData
	rentOption, rentPlan := libs.GetOption(rentid)
	exed = conv(exed, rentOption, rentPlan)
	res := resconv(exed)
	fmt.Println(res)
	byt, _ := json.Marshal(res)
	g.Writer.Write(byt)
}

func resconv(res model.ExecData) resdata {
	var conved resdata
	conved.Onehouse = res.Onehouse
	conved.Japaneseroom = res.Japaneseroom
	conved.Bedtype = res.Bedtype
	conved.Wifi = res.Wifi
	conved.Guesthouse = res.Guesthouse
	conved.Japaneseroom6 = res.Japaneseroom6
	conved.Singleroom = res.Singleroom
	conved.Kitchen = res.Kitchen
	conved.Kayabuki = res.Kayabuki
	conved.Japaneseroom1 = res.Japaneseroom1
	conved.Shower = res.Shower
	conved.Aircon = res.Aircon
	conved.Kawarabuki = res.Kawarabuki
	conved.Japaneseroom3 = res.Japaneseroom3
	conved.Bath = res.Bath
	conved.Tv = res.Tv
	conved.Reserved = res.Reserved
	conved.Explain = res.Explain
	conved.Traincamp = res.Traincamp
	if res.Closetoilet {
		conved.Domitory = true
	}
	if res.Weakheart {
		conved.Near = true
		conved.Domitory = true
	}
	if res.Weakleg {
		conved.Floor = true
	}
	conved.Plans = res.Plans
	return conved
}

// Conv execute convertion
func conv(res model.ExecData, origin []interface{}, plan [][]interface{}) model.ExecData {
	res.Onehouse = tobool(origin[0])
	res.Guesthouse = tobool(origin[1])
	res.Japaneseroom = tobool(origin[2])
	res.Japaneseroom6 = tobool(origin[3])
	res.Japaneseroom1 = tobool(origin[4])
	res.Japaneseroom3 = tobool(origin[5])
	res.Bedtype = tobool(origin[6])
	res.Singleroom = tobool(origin[7])
	res.Shower = tobool(origin[8])
	res.Bath = tobool(origin[9])
	res.Wifi = tobool(origin[10])
	res.Kitchen = tobool(origin[11])
	res.Aircon = tobool(origin[12])
	res.Tv = tobool(origin[13])
	res.Weakleg = tobool(origin[14])
	res.Closetoilet = tobool(origin[15])
	res.Reserved = tobool(origin[16])
	res.Traincamp = tobool(origin[17])
	res.Weakheart = tobool(origin[18])
	res.Explain = tostring(origin[19])
	res.Kayabuki = tobool(origin[20])
	res.Kawarabuki = tobool(origin[21])
	res.Name = tostring(origin[22])
	for _, ary := range plan {
		res.Plans = append(res.Plans, tostring(ary[0]))
	}
	return res
}

func tostring(arg interface{}) string {
	return arg.(string)
}

func tobool(arg interface{}) bool {
	return arg.(bool)
}
