package service

import (
	"encoding/json"
	"hamahakuaccomodation/src/backend/libs"
	"hamahakuaccomodation/src/backend/model"

	"github.com/gin-gonic/gin"
)

func calc(origin model.Data, name string) model.ExecData {
	var cp model.ExecData
	if origin.Domitory {
		cp.Weakheart = true
		cp.Closetoilet = true
	}
	if origin.Near {
		cp.Weakheart = true
	}
	if origin.Floor {
		cp.Weakleg = true
	}
	cp.Onehouse = origin.Onehouse
	cp.Guesthouse = origin.Guesthouse
	cp.Japaneseroom = origin.Japaneseroom
	cp.Japaneseroom6 = origin.Japaneseroom6
	cp.Japaneseroom3 = origin.Japaneseroom3
	cp.Japaneseroom1 = origin.Japaneseroom1
	cp.Bedtype = origin.Bedtype
	cp.Singleroom = origin.Singleroom
	cp.Shower = origin.Shower
	cp.Bath = origin.Bath
	cp.Wifi = origin.Wifi
	cp.Kitchen = origin.Kitchen
	cp.Aircon = origin.Aircon
	cp.Tv = origin.Tv
	cp.Reserved = origin.Reserved
	cp.Traincamp = origin.Traincamp
	cp.Explain = origin.Explain
	cp.Kayabuki = origin.Kayabuki
	cp.Kawarabuki = origin.Kawarabuki
	cp.Name = name
	cp.Plans = origin.Plans
	return cp
}

// AccomodationUpdate update accomodation's option
func AccomodationUpdate(g *gin.Context) {
	req := g.Request
	req.ParseForm()
	jsonstring := req.PostFormValue("data")
	name := req.PostFormValue("name")
	var reqdata model.Data
	json.Unmarshal(([]byte)(jsonstring), &reqdata)
	execdata := calc(reqdata, name)
	libs.UpdateRent(execdata, "2NvO2dDL8uxfjlv")
	libs.UpdatePlan(execdata.Plans, "2NvO2dDL8uxfjlv")
}
