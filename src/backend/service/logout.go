package service

import (
	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// Logout process log out
func Logout(g *gin.Context) {
	session := sessions.Default(g)
	session.Clear()
	session.Save()
}
