module ./

require (
	github.com/boj/redistore v0.0.0-20180917114910-cd5dcc76aeff // indirect
	github.com/gin-gonic/contrib v0.0.0-20190923054218-35076c1b2bea // indirect
	github.com/gin-gonic/gin v1.4.0 // indirect
	github.com/gorilla/context v1.1.1 // indirect
	github.com/gorilla/sessions v1.2.0 // indirect
	github.com/lib/pq v1.2.0 // indirect
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646 // indirect
	github.com/olahol/go-imageupload v0.0.0-20160503070439-09d2b92fa05e // indirect
)
