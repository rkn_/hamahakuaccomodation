package model

// Data is request data
type Data struct {
	Onehouse      bool
	Japaneseroom  bool
	Bedtype       bool
	Wifi          bool
	Guesthouse    bool
	Japaneseroom6 bool
	Singleroom    bool
	Kitchen       bool
	Kayabuki      bool
	Japaneseroom1 bool
	Shower        bool
	Aircon        bool
	Kawarabuki    bool
	Japaneseroom3 bool
	Bath          bool
	Tv            bool
	Floor         bool
	Reserved      bool
	Near          bool
	Traincamp     bool
	Domitory      bool
	Explain       string
	Plans         []string
}

// ExecData use execute update DB
type ExecData struct {
	Onehouse      bool
	Japaneseroom  bool
	Bedtype       bool
	Wifi          bool
	Guesthouse    bool
	Japaneseroom6 bool
	Singleroom    bool
	Kitchen       bool
	Kayabuki      bool
	Japaneseroom1 bool
	Shower        bool
	Aircon        bool
	Kawarabuki    bool
	Japaneseroom3 bool
	Bath          bool
	Tv            bool
	Reserved      bool
	Traincamp     bool
	Explain       string
	Weakleg       bool
	Reservedroom  bool
	Closetoilet   bool
	Weakheart     bool
	Name          string
	Plans         []string
}
