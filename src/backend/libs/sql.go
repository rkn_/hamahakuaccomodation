package libs

import (
	"fmt"
	"hamahakuaccomodation/src/backend/model"
	"time"

	// pq needs to connect postgres DB
	_ "github.com/lib/pq"
)

// ResisterUser is called by signup process
func ResisterUser(name, mail, tel, passwd string) (bool, string) {
	id := GenerateID()
	rentsRes := insertRents(id, name, mail, tel, passwd)
	rentRes := insertRent(id, name)
	if rentsRes && rentRes {
		return true, id
	}
	return false, ""
}

func insertRents(id, accomodationname, mail, tel, passwd string) bool {
	query := "insert into rents (rentid, email, tel, name, passwd) values ($1, $2, $3, $4, $5)"
	args := []interface{}{id, mail, tel, accomodationname, passwd}
	return exec(query, args)
}

// exec is transaction process
func exec(query string, args []interface{}) bool {
	db := Connection()
	defer db.Close()

	tx, _ := db.Begin()
	_, err := tx.Exec(query, args...)
	success := false
	if err != nil {
		tx.Rollback()
	} else {
		tx.Commit()
		success = true
	}
	return success
}

func getRow(query string, args []interface{}) [][]interface{} {
	db := Connection()
	defer db.Close()
	tx, _ := db.Begin()
	rows, err := tx.Query(query, args...)
	col, _ := rows.Columns()
	colnum := len(col)
	var res [][]interface{}
	for rows.Next() {
		elm := make([]interface{}, colnum)
		ptr := make([]interface{}, colnum)
		for i := range col {
			ptr[i] = &elm[i]
		}
		rows.Scan(ptr...)
		res = append(res, elm)
	}
	defer tx.Commit()
	if err != nil {
		tx.Rollback()
		return nil
	}
	return res
}

func insertRent(id, accomodationname string) bool {
	db := Connection()
	defer db.Close()

	query := "insert into rent (rentid, " +
		"onehouse, " +
		"guesthouse, " +
		"jproom, " +
		"jproom6, " +
		"jproom1, " +
		"jproom3woman, " +
		"bedtype, " +
		"singlewoman, " +
		"shower, " +
		"bath, " +
		"wifi, " +
		"kitchen, " +
		"aircon, " +
		"tv, " +
		"weaklegs, " +
		"closetoilet, " +
		"reservedroom, " +
		"traincamp, " +
		"weakheart, " +
		"explain, " +
		"kayabuki, " +
		"kawarabuki, " +
		"name) values ($1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15, $16, $17, $18, $19, $20, $21, $22, $23, $24)"
	args := []interface{}{id,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		false,
		"",
		false,
		false,
		accomodationname}
	done := exec(query, args)
	return done
}

// UpdateRent update psql db table rent
func UpdateRent(data model.ExecData, userid string) {
	query := "update rent set onehouse = $1, " +
		"guesthouse = $2, " +
		"jproom = $3, " +
		"jproom6 = $4, " +
		"jproom1 = $5, " +
		"jproom3woman = $6, " +
		"bedtype = $7, " +
		"singlewoman = $8, " +
		"shower = $9, " +
		"bath = $10, " +
		"wifi = $11, " +
		"kitchen = $12, " +
		"aircon = $13, " +
		"tv = $14, " +
		"weaklegs = $15, " +
		"closetoilet = $16, " +
		"reservedroom = $17, " +
		"traincamp = $18, " +
		"weakheart = $19, " +
		"explain = $20, " +
		"kayabuki = $21, " +
		"kawarabuki = $22, " +
		"name = $23 where rentid = $24"
	args := []interface{}{data.Onehouse,
		data.Guesthouse,
		data.Japaneseroom,
		data.Japaneseroom6,
		data.Japaneseroom1,
		data.Japaneseroom3,
		data.Bedtype,
		data.Singleroom,
		data.Shower,
		data.Bath,
		data.Wifi,
		data.Kitchen,
		data.Aircon,
		data.Tv,
		data.Weakleg,
		data.Closetoilet,
		data.Reserved,
		data.Traincamp,
		data.Weakheart,
		data.Explain,
		data.Kayabuki,
		data.Kawarabuki,
		data.Name,
		userid}
	exec(query, args)
}

// UpdatePlan update rent plan
func UpdatePlan(plans []string, rentid string) {
	// 今までのプランを削除
	query := "delete from rent_plan where rentid = $1"
	args := []interface{}{rentid}
	exec(query, args)

	query = "insert into rent_plan (rentid, explain, plan) values ($1, $2, $3)"
	for _, val := range plans {
		args = []interface{}{rentid, "", val}
		exec(query, args)
	}
}

// GetOption get option accomdation
func GetOption(rentid string) ([]interface{}, [][]interface{}) {
	query := "SELECT rent.onehouse, " +
		"rent.guesthouse, " +
		"rent.jproom, " +
		"rent.jproom6, " +
		"rent.jproom1, " +
		"rent.jproom3woman, " +
		"rent.bedtype, " +
		"rent.singlewoman, " +
		"rent.shower, " +
		"rent.bath, " +
		"rent.wifi, " +
		"rent.kitchen, " +
		"rent.aircon, " +
		"rent.tv, " +
		"rent.weaklegs, " +
		"rent.closetoilet, " +
		"rent.reservedroom, " +
		"rent.traincamp, " +
		"rent.weakheart, " +
		"rent.explain, " +
		"rent.kayabuki, " +
		"rent.kawarabuki, " +
		"rent.name  FROM rent WHERE rent.rentid = $1"
	args := []interface{}{rentid}
	rows := getRow(query, args)[0]
	query = "select rent_plan.plan from rent_plan where rent_plan.rentid = $1"
	planrows := getRow(query, args)
	return rows, planrows
}

// InitOpen initialize open date
func InitOpen(rentid string) {
	query := "delete from open where rentid = $1"
	args := []interface{}{rentid}
	exec(query, args)
}

// InsertOpen update Open
func InsertOpen(rentid, date string, room int) {
	fmt.Println(rentid)
	fmt.Println(date)
	query := "insert into open (rentid, date, rooms) values ($1, $2, $3)"
	args := []interface{}{rentid, date, room}
	exec(query, args)
}

// GetOpen get open
func GetOpen(rentid string) [][]interface{} {
	query := "select TO_CHAR(date, 'YYYY/MM/DD'), rooms from open where rentid = $1"
	args := []interface{}{rentid}
	data := getRow(query, args)
	return data
}

// GetReservation get reservation
func GetReservation(rentid string) [][]interface{} {
	query := "select userid, rooms, checkin, checkout, man, woman from reservation where rentid = $1 and checkin >= $2"
	t := time.Now()
	layout := "2006/01/02"
	now := t.Format(layout)
	args := []interface{}{rentid, now}
	reservation := getRow(query, args)
	return reservation
}

// GetReservationinfo get reservation info
func GetReservationinfo(userid, rentid string) [][]interface{} {
	query := "select alchohole, english, haral, vegan, vegetarian, allergies, plan, weakheart, weaklegs, closetoilet, reserved, traincamp from resister_info where userid = $1 and rentid = $2"
	args := []interface{}{userid, rentid}
	info := getRow(query, args)
	return info
}

// GetUserName get user name by userid
func GetUserName(userid string) string {
	args := []interface{}{userid}
	query := "select firstname, lastname from users where userid = $1"
	username := getRow(query, args)[0][0].(string) + " " + getRow(query, args)[0][1].(string)
	return username
}

// GetUserMail get user mail by userid
func GetUserMail(userid string) string {
	args := []interface{}{userid}
	query := "select email from users where userid = $1"
	mail := getRow(query, args)[0][0].(string)
	return mail
}

// GetAuthElm get password, rentid
func GetAuthElm(mail string) (string, string) {
	args := []interface{}{mail}
	query := "select passwd, rentid from rents where email=$1"
	res := getRow(query, args)
	if len(res) == 0 {
		return "", ""
	}
	return res[0][0].(string), res[0][1].(string)
}
