package libs

import (
	"database/sql"
	// pq needs to connect postgres DB
	_ "github.com/lib/pq"
)

// Connection method connect db
func Connection() *sql.DB {
	// local
	dummy := "dummy"
	db, err := sql.Open("postgres", "host=127.0.0.1 port=5432 user=lkeix password=" + dummy + " dbname=hamahaku sslmode=disable")
	if err != nil {
		panic(err)
	}
	return db
}
