package router

import (
	"hamahakuaccomodation/src/backend/service"
	"os"

	"github.com/gin-gonic/contrib/sessions"
	"github.com/gin-gonic/gin"
)

// CreateRoute create routing api , service
func CreateRoute() *gin.Engine {
	router := gin.Default()

	store := sessions.NewCookieStore([]byte("secret"))
	router.Use(sessions.Sessions("session", store))
	gopath := os.Getenv("GOPATH")
	router.LoadHTMLFiles(gopath + "/src/hamahakuaccomodation/dist/index.html")
	router.Static("/css", gopath+"/src/hamahakuaccomodation/dist/css")
	router.Static("/js", gopath+"/src/hamahakuaccomodation/dist/js")
	router.Static("/data", gopath+"/src/hamahakuaccomodation/data")
	router.GET("/", view)

	serve := router.Group("/hamahakuaccomodation/service")
	{
		serve.POST("/signup", service.Signup)
		serve.POST("/accomodationUpdate", service.AccomodationUpdate)
		serve.POST("/getOption", service.GetOption)
		serve.POST("/imgstore", service.ImageStore)
		serve.POST("/getimg", service.Getimg)
		serve.POST("/updateopen", service.UpdateOpen)
		serve.POST("/getExert", service.GetExert)
		serve.POST("/getreservation", service.GetReservation)
		serve.POST("/login", service.Auth)
		serve.POST("/sessauth", service.SessAuth)
		serve.POST("/logout", service.Logout)
	}

	router.Group("/hamahakuaccomodation/api/")
	{

	}

	return router
}

func view(g *gin.Context) {
	g.HTML(200, "index.html", gin.H{})
}
