import Vue from 'vue'
import Vuex from 'vuex'
import createPersistedState from "vuex-persistedstate"

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        auth: {
            login: false,
            sessid: ''
        }
    },
    mutations: {
      login(state, sessid){
        state.auth.login = true
        state.auth.sessid = sessid
      },
      logout(state){
          state.auth.login = false
          state.auth.sessid = ''
      }
    },
    getters: {
      getlogin(state){
        return state.auth.login
      },
      getsessid(state) {
        return state.auth.sessid
      }
    },
    actions: {

    },
    plugins: [createPersistedState({storage: window.sessionStorage})]  
  })