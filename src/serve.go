package main

import "hamahakuaccomodation/src/backend/router"

func main() {
	route := router.CreateRoute()
	route.Run(":8080")
}
